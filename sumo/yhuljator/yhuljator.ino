#define TRIG A5
#define ECHO A4

#define DIR1 12
#define POW1 10
#define DIR2 13
#define POW2 11

#define FOUND_DISTANCE 35
int pulseTimeOut = (FOUND_DISTANCE+1)*2*29.1;
void setup() {
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);

  pinMode(DIR1, OUTPUT);
  pinMode(POW1, OUTPUT);
  pinMode(DIR2, OUTPUT);
  pinMode(POW2, OUTPUT);

  digitalWrite(TRIG, LOW);
  digitalWrite(DIR1, LOW);
  digitalWrite(POW1, LOW);
  digitalWrite(DIR2, LOW);
  digitalWrite(POW2, LOW);
}

long time, dist;
void loop() {
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(5);
  digitalWrite(TRIG, LOW);
  time = pulseIn(ECHO, digitalRead(ECHO) == HIGH ? LOW : HIGH, pulseTimeOut);
  dist = (time / 2) / 29.1; // divide by 29.1 or multiply by 0.0343
  
  if(dist<FOUND_DISTANCE){
    digitalWrite(DIR1, HIGH);
    digitalWrite(POW1, LOW);
    digitalWrite(DIR2, LOW);
    digitalWrite(POW2, HIGH);
  }else{
    digitalWrite(DIR1, HIGH);
    digitalWrite(POW1, LOW);
    digitalWrite(DIR2, HIGH);
    digitalWrite(POW2, LOW);
  }
  delay(100);

  digitalWrite(DIR1, LOW);
  digitalWrite(POW1, LOW);
  digitalWrite(DIR2, LOW);
  digitalWrite(POW2, LOW);
}
