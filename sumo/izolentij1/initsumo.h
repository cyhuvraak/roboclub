//ultrasonic sensor pins
#define TRIG A5
#define ECHO A4

//color sensors output pins. Note configuration pins for 3 sensors are connected in paralel
#define S0 2
#define S1 3
#define S2 4
#define S3 5

//color sensors output pins. value greater than 250 is interpreted as black color.
#define OUT1 6// left
#define OUT2 7// right
#define OUT3 8// rear

//motor driver pins
#define DIR1 12
#define POW1 10
#define DIR2 A3
#define POW2 11

//speaker output
#define MOUT 9

//time in ms while robot move without checking distance during atack
#define UNCHECK_MOVE_MS 200
//time in ms for calibration move during rotation
#define CLB_DL_STEP 200
//num of rotation steps in opposite direction before search rotation
#define CLB_DL_STEP_COUNT 25
//max distance in cm to attack object
#define FOUND_DISTANCE 100
//time out for ulrasonic responce wait
int pulseTimeOut = (FOUND_DISTANCE+1)*2*29.1;

//each mikrosecond check color sensors state
#define COLOR_CHEKC_MKS 200
//Sensitivity of capacitive sensor
#define SENS 1000

//notes frequency difinition//////////////////////////////////////////////////////////////////////////////////////
#define NOTEC0 61162
#define NOTEC0H 57737
#define NOTED0 54496
#define NOTED0H 51414
#define NOTEE0 48544
#define NOTEF0 45809
#define NOTEF0H 43253
#define NOTEG0 40816
#define NOTEG0H 38521
#define NOTEA0 36364
#define NOTEA0H 34317
#define NOTEB0 32394
#define NOTEC1 30581
#define NOTEC1H 28860
#define NOTED1 27241
#define NOTED1H 25714
#define NOTEE1 24272
#define NOTEF1 22910
#define NOTEF1H 21622
#define NOTEG1 20408
#define NOTEG1H 19264
#define NOTEA1 18182
#define NOTEA1H 17161
#define NOTEB1 16197
#define NOTEC2 15288
#define NOTEC2H 14430
#define NOTED2 13620
#define NOTED2H 12857
#define NOTEE2 12134
#define NOTEF2 11453
#define NOTEF2H 10811
#define NOTEG2 10204
#define NOTEG2H 9631
#define NOTEA2 9091
#define NOTEA2H 8581
#define NOTEB2 8099
#define NOTEC3 7645
#define NOTEC3H 7216
#define NOTED3 6811
#define NOTED3H 6428
#define NOTEE3 6068
#define NOTEF3 5727
#define NOTEF3H 5405
#define NOTEG3 5102
#define NOTEG3H 4816
#define NOTEA3 4545
#define NOTEA3H 4290
#define NOTEB3 4050
#define NOTEC4 3822
#define NOTEC4H 3608
#define NOTED4 3405
#define NOTED4H 3214
#define NOTEE4 3034
#define NOTEF4 2863
#define NOTEF4H 2703
#define NOTEG4 2551
#define NOTEG4H 2408
#define NOTEA4 2273
#define NOTEA4H 2145
#define NOTEB4 2025
#define NOTEC5 1911
#define NOTEC5H 1804
#define NOTED5 1703
#define NOTED5H 1607
#define NOTEE5 1517
#define NOTEF5 1432
#define NOTEF5H 1351
#define NOTEG5 1276
#define NOTEG5H 1204
#define NOTEA5 1136
#define NOTEA5H 1073
#define NOTEB5 1012
#define NOTEC6 956
#define NOTEC6H 902
#define NOTED6 851
#define NOTED6H 804
#define NOTEE6 758
#define NOTEF6 716
#define NOTEF6H 676
#define NOTEG6 638
#define NOTEG6H 602
#define NOTEA6 568
#define NOTEA6H 536
#define NOTEB6 506
#define NOTEC7 478
#define NOTEC7H 451
#define NOTED7 426
#define NOTED7H 402
#define NOTEE7 379
#define NOTEF7 358
#define NOTEF7H 338
#define NOTEG7 319
#define NOTEG7H 301
#define NOTEA7 284
#define NOTEA7H 268
#define NOTEB7 253
#define NOTEC8 239
#define NOTEC8H 225
#define NOTED8 213
#define NOTED8H 201
#define NOTEE8 190
#define NOTEF8 179
#define NOTEF8H 169
#define NOTEG8 159
#define NOTEG8H 150
#define NOTEA8 142
#define NOTEA8H 134
#define NOTEB8 127

#define  R     0
//////////////////////////////////////////////////////////////////////////////////////////////


long pulseTime, dist;
//capacitive buttons
CapacitiveSensor   stopCapas = CapacitiveSensor(A1, A2);
CapacitiveSensor   changeCapas = CapacitiveSensor(A1, A0);

//melody definitions////////////////////////////////////////////////////////////////
int melody[] = { NOTEB2, R, NOTEB2, R, NOTEB2, R, NOTED3, R, NOTEE3, R, NOTEE3, R, NOTEF3, R, NOTEF3H, R, NOTEA3, R, NOTEA3H, R, NOTEB3, R, R, R, R, NOTEF2H, R, NOTEE2, NOTEC2H, R, NOTEC2, R, NOTEB2, R, NOTEE3, R, NOTEB3, R, NOTEB3, NOTEC3H, R, NOTEC3H};
int beats[]  = { 200,    1, 200,    1, 200,    1, 200,    1, 200,    1, 200,    1, 200,    1, 200,     1, 200,    1, 200,     1, 200,   1, 1, 1, 1, 200,     1, 200,    200,     1, 200,    1, 200,    1, 200,    1, 200,    1, 100,    100,     1, 200,   };
int MAX_COUNT = sizeof(melody) / 2; // Melody length, for looping.

long tempo = 800;
int rest_count = 100;

int tone_ = 0;
int beat = 0;
long duration  = 0;
////////////////////////////////////////////////////////////////////////////////////
void setup() {
	//	Serial.begin(9600);
	//	Serial.print("init");

	//setup pins mode
	pinMode(TRIG, OUTPUT);
	pinMode(ECHO, INPUT);

	pinMode(S0, OUTPUT);
	pinMode(S1, OUTPUT);
	pinMode(S2, OUTPUT);
	pinMode(S3, OUTPUT);
	pinMode(OUT1, INPUT);
	pinMode(OUT2, INPUT);
	pinMode(OUT3, INPUT);

	pinMode(DIR1, OUTPUT);
	pinMode(POW1, OUTPUT);
	pinMode(DIR2, OUTPUT);
	pinMode(POW2, OUTPUT);

	//setup color sensor to grey mode and 75% frequency
	digitalWrite(S0, LOW);
	digitalWrite(S1, HIGH);
	digitalWrite(S2, HIGH);
	digitalWrite(S3, LOW);

	//setup speaker
	pinMode(MOUT, OUTPUT);
	digitalWrite(MOUT, LOW);

	//reset ultrasonic and motors
	digitalWrite(TRIG, LOW);
	digitalWrite(DIR1, LOW);
	digitalWrite(POW1, LOW);
	digitalWrite(DIR2, LOW);
	digitalWrite(POW2, LOW);
}
