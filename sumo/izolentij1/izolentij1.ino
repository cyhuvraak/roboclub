#include <CapacitiveSensor.h>
#include "initsumo.h"

//main loop, waits until button is pressed, plays melody with play() function and goes into main algotithm in sumo() function.
void loop() {
	long st =  stopCapas.capacitiveSensor(30);
	if (st > SENS) {
		play();
		sumo();
	}
}
//plays a melody for about 5 sec.
void play() {
	for (int i = 0; i < MAX_COUNT; i++) {
		tone_ = melody[i];
		beat = beats[i];
		duration = beat * tempo;

		playTone();
	}
}
//plays current note
void playTone() {
	long elapsed_time = 0;
	if (tone_ > 0) {
		while (elapsed_time < duration) {
			digitalWrite(MOUT, HIGH);
			delayMicroseconds(tone_ / 2);
			digitalWrite(MOUT, LOW);
			delayMicroseconds(tone_ / 2);
			elapsed_time += (tone_);
		}
	}
	else { // Rest beat; loop times delay
		for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
			delayMicroseconds(duration);
		}
	}
}
//returns true if find object in less than FOUND_DISTANCE in front of robot
bool gotIt() {
	digitalWrite(TRIG, HIGH);
	delayMicroseconds(5);
	digitalWrite(TRIG, LOW);
	pulseTime = pulseIn(ECHO, digitalRead(ECHO) == HIGH ? LOW : HIGH, pulseTimeOut);
	dist = (pulseTime / 2) / 29.1; // divide by 29.1 or multiply by 0.0343
	if (0 < pulseTime && dist < FOUND_DISTANCE) return true;

	//wait for returning signal from big distances and double check if oponent robot is in angle that forwards signal.
	myDelay(100,true);
	digitalWrite(TRIG, HIGH);
	delayMicroseconds(5);
	digitalWrite(TRIG, LOW);
	pulseTime = pulseIn(ECHO, digitalRead(ECHO) == HIGH ? LOW : HIGH, pulseTimeOut);
	dist = (pulseTime / 2) / 29.1; // divide by 29.1 or multiply by 0.0343
	return (0 < pulseTime && dist < FOUND_DISTANCE);
}

//main algorithm
void sumo() {
	search();
	for (;;) {
		if (gotIt()) {
			move(UNCHECK_MOVE_MS, true, false, false);
		} else {
			move(CLB_DL_STEP, false, true, true);
			int i;
			for (i = 0; i <= CLB_DL_STEP_COUNT; i++) {
				if (gotIt()) {
					foundMove(true);  
					break;
				}
				move(CLB_DL_STEP, false, true, true);
			}
			if (i > CLB_DL_STEP_COUNT) {
				search();
			}
		}
	}
}

//attack!
void foundMove(bool dir/*oposite balance move*/) {
	move(CLB_DL_STEP, dir, true, false);
	move(UNCHECK_MOVE_MS, true, false, false);
}

//rotate and search
void search() {
	for (;;) {
		if (gotIt()) {
			foundMove(false);
			return;
		} else {
			move(CLB_DL_STEP, true, true, true);
		}
	}
}
//move function for time in ms, dirrection dir, straigt or turning...
void move(int ms, bool dir, bool turn, bool stopInTheEnd) {
	if (turn) {
		digitalWrite(DIR1, dir);
		digitalWrite(POW1, !dir);
		digitalWrite(DIR2, !dir);
		digitalWrite(POW2, dir);
	} else {
		digitalWrite(DIR1, dir);
		digitalWrite(POW1, !dir);
		digitalWrite(DIR2, dir);
		digitalWrite(POW2, !dir);
	}
	myDelay(ms, dir);
	if (stopInTheEnd) {
		stopMove();
	}
}

//my delay slightly bigger but checks color sensors and tries to escape in case of border
void myDelay(long ms, bool dir) {
	long rest = ms;
	rest *= 250;
	for (long i = 0; i < ms; i++) {
		if (i % COLOR_CHEKC_MKS == 0) {
			if (checkColor(dir)) {
				return;
			}
		}
		delayMicroseconds(1);
	}
}

void stopMove() {
	digitalWrite(DIR1, LOW);
	digitalWrite(POW1, LOW);
	digitalWrite(DIR2, LOW);
	digitalWrite(POW2, LOW);
}

bool checkColor(boolean dir) {
	bool rslt = false;
	if ( pulseIn(OUT3, digitalRead(OUT3) == HIGH ? LOW : HIGH) > 250 ) {
		rslt = true;
		digitalWrite(DIR1, dir);
		digitalWrite(POW1, !dir);
		digitalWrite(DIR2, !dir);
		digitalWrite(POW2, dir);
		delay(100);
		forward(500);
	}
	if ( pulseIn(OUT1, digitalRead(OUT1) == HIGH ? LOW : HIGH) > 250 ) {
		if ( pulseIn(OUT2, digitalRead(OUT2) == HIGH ? LOW : HIGH) > 250 ){
			backward(500);
		}
		rslt = true;
		digitalWrite(DIR2, 1);
		digitalWrite(POW2, 0);
		digitalWrite(DIR1, 0);
		digitalWrite(POW1, 1);
		delay(200);
		forward(1);
	}
	if ( pulseIn(OUT2, digitalRead(OUT2) == HIGH ? LOW : HIGH) > 250 ) {
		rslt = true;
		digitalWrite(DIR1, 1);
		digitalWrite(POW1, 0);
		digitalWrite(DIR2, 0);
		digitalWrite(POW2, 1);
		delay(200);
		forward(1);
	}
	return rslt;
}

void backward(long ms) {
	digitalWrite(DIR1, 0);
	digitalWrite(POW1, 1);
	digitalWrite(DIR2, 0);
	digitalWrite(POW2, 1);
	delay(ms);
}

void forward(long ms) {
	digitalWrite(DIR1, 1);
	digitalWrite(POW1, 0);
	digitalWrite(DIR2, 1);
	digitalWrite(POW2, 0);
	delay(ms);
}
