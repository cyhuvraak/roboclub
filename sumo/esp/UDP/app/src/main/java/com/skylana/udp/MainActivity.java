package com.skylana.udp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;

public class MainActivity extends AppCompatActivity {
	//TODO: refactor!
    String address = "255.255.255.255"; //broadcating wont work use multicast

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);

        final Button butt = (Button) findViewById(R.id.button);
        butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Enumeration en = NetworkInterface.getNetworkInterfaces();
                    final ArrayList<String> addrss = new ArrayList<String>();

                    while (en.hasMoreElements()) {
                        NetworkInterface ni = (NetworkInterface) en.nextElement();
                        Enumeration ee = ni.getInetAddresses();
                        while (ee.hasMoreElements()) {
							//TODO: Manage ipv6
                            InetAddress ia = (InetAddress) ee.nextElement();
                            if (ia.getHostAddress().indexOf('.') > 0)
                                addrss.add(ia.getHostAddress());
                        }
                    }

                    CharSequence interfaces[] = addrss.toArray(new CharSequence[addrss.size()]);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Pick a network");
                    builder.setItems(interfaces, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String adress = addrss.get(which);
							//TODO: Get network mask to get correct multicast address for complex masks
                            address=adress.substring(0, adress.lastIndexOf('.'))+".255";
                        }
                    });
                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        final SeekBar skl = (SeekBar) findViewById(R.id.seekBarL);
        final SeekBar skr = (SeekBar) findViewById(R.id.seekBarR);
        try {
            final InetAddress IP = InetAddress.getLocalHost();
            final DatagramSocket socket = new DatagramSocket(9988);
            socket.setBroadcast(true);

            skl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    skl.setProgress(255);
                    for(int i=0;i<10;i++) {
                        sendProgress(0, socket, "l");
                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    sendProgress(progress-255, socket,"l");
                }
            });
            skr.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    skr.setProgress(255);
                    for(int i=0;i<10;i++) {
                        sendProgress(0, socket, "r");
                        try {
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    sendProgress(progress-255, socket,"r");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendProgress(int progress, DatagramSocket socket, String dir) {
        try {
            StringBuilder data = new StringBuilder().append(dir);
            if(progress<0){
                data.append('-');
            }else{
                data.append(0);
            }
            if(Math.abs(progress)<100){
                data.append(0);
            }
            if(Math.abs(progress)<10){
                data.append(0);
            }
            data.append(Math.abs(progress));
            byte[] sendData = data.toString().getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(address), 9988);
            socket.send(sendPacket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
