#include <ESP8266WiFi.h>
#include <WiFiUdp.h>


const char *ssid = "network_to_connect";
const char *password = "password";

WiFiUDP udp;

IPAddress broadcastIp;//(255, 255, 255, 255) broadcat is not working, so will use multicast

void setup() {
  Serial.begin(115200);
  delay(5000);//Time to init serial and open monitor to debug

  //start wifi
  WiFi.begin(ssid, password);

  //wait to connect
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //get multicat ip and output it
  broadcastIp = ~WiFi.subnetMask() | WiFi.gatewayIP();
  Serial.println("");
  Serial.println(broadcastIp);

  //listen for messages
  udp.begin(9988);
}
String str;
char packetBuffer[5];
void loop() {
  //receive info from network and pass it further through serial
  if (udp.parsePacket()) {
    udp.read(packetBuffer, 5);
    Serial.println(packetBuffer);
  }

  //send info from serial to network if awailable
  if (Serial.available() > 0)  {
    str = Serial.readStringUntil('\n');
    udp.beginPacket(broadcastIp, 9989);
    udp.print(str);
    udp.endPacket();
  }
}

