//Піни до якийх підключено ультразвуковий сенсор
#define TRIG A5
#define ECHO A4

//Піни до якийх підключено сенсори кольору.
//В даній схемі конфігураційні піни підключаються паралельно для всіх трьох сенсорів.
#define S0 2
#define S1 3
#define S2 4
#define S3 5
//Піни данних кольорів сенсору
#define OUT1 6
#define OUT2 7
#define OUT3 8

//Піни драйвера
#define DIR1 12
#define POW1 10
#define DIR2 13
#define POW2 11

void setup() {
  //Включаєм передачу даних.
  Serial.begin (9600);

  //Налаштування пінів ультразвукового сенсору
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);

  //Налаштування пінів кольорових сенсорів
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(OUT1, INPUT);
  pinMode(OUT2, INPUT);
  pinMode(OUT3, INPUT);

  //Налаштування пінів драйвера
  pinMode(DIR1, OUTPUT);
  pinMode(POW1, OUTPUT);
  pinMode(DIR2, OUTPUT);
  pinMode(POW2, OUTPUT);

  //Встановлення тригера в нульовий стан
  digitalWrite(TRIG, LOW);

  //Налаштування частоти та сприйняття сірого кольору для сенсорів кольору
  digitalWrite(S0, LOW);
  digitalWrite(S1, HIGH);
  digitalWrite(S2, HIGH);
  digitalWrite(S3, LOW);

  //Вимкнення двигунів
  digitalWrite(DIR1, LOW);
  digitalWrite(POW1, LOW);
  digitalWrite(DIR2, LOW);
  digitalWrite(POW2, LOW);
}

long time, dist, grey;
void loop() {
  //Запуск ультразвукового сигналу
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(5);
  digitalWrite(TRIG, LOW);
  //Зчитування часу відгуку
  time = pulseIn(ECHO, digitalRead(ECHO) == HIGH ? LOW : HIGH);
  //Обрахування відстані
  dist = (time / 2) / 29.1; // divide by 29.1 or multiply by 0.0343
  //Вивід даних
  Serial.print(time);
  Serial.println("time");
  Serial.print(dist);
  Serial.println("cm");

  //Зчитування даних з першого сенсору кольору
  grey = pulseIn(OUT1, digitalRead(OUT1) == HIGH ? LOW : HIGH);
  //Вивід даних
  Serial.print(grey);
  Serial.println(" shades of grey on OUT1");

  //Зчитування даних з другого сенсору кольору
  grey = pulseIn(OUT2, digitalRead(OUT2) == HIGH ? LOW : HIGH);
  //Вивід даних
  Serial.print(grey);
  Serial.println(" shades of grey on OUT2");

  //Зчитування даних з третього сенсору кольору
  grey = pulseIn(OUT3, digitalRead(OUT3) == HIGH ? LOW : HIGH);
  //Вивід даних
  Serial.print(grey);
  Serial.println(" shades of grey on OUT3");


  //Тестування двигунів з рухом в різні сторони
  Serial.println("moving forward 100%");
  digitalWrite(DIR1, HIGH);
  digitalWrite(POW1, LOW);
  digitalWrite(DIR2, HIGH);
  digitalWrite(POW2, LOW);
  delay(2000);

  Serial.println("moving backward 100%");
  digitalWrite(DIR1, LOW);
  digitalWrite(POW1, HIGH);
  digitalWrite(DIR2, LOW);
  digitalWrite(POW2, HIGH);
  delay(2000);

  Serial.println("moving forward 50%");
  digitalWrite(DIR1, HIGH);
  analogWrite(POW1, 128);
  digitalWrite(DIR2, HIGH);
  analogWrite(POW2, 128);
  delay(2000);

  Serial.println("moving backward 50%");
  digitalWrite(DIR1, LOW);
  analogWrite(POW1, 128);
  digitalWrite(DIR2, LOW);
  analogWrite(POW2, 128);
  delay(2000);

  //Зупинка двигунів
  Serial.println("stop");
  digitalWrite(DIR1, LOW);
  digitalWrite(POW1, LOW);
  digitalWrite(DIR2, LOW);
  digitalWrite(POW2, LOW);
}
