package com.skylana.ev3controll;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;

/**
 * Created by yskalskyy on 26.11.2015.
 */
public class ControllService extends Service {

    private static final String TAG = "ControllService";
    private static final byte[] header = {0, 1, 0, -127, -98, 2, 48, 0};

    private boolean isRunning = false;

    @Override
    public void onCreate() {
        isRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        connectWebSocket();
        return Service.START_STICKY;
    }

    private void writeMsg(OutputStream outputStream, String str) throws Exception {
        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        byte[] content = str.getBytes();
        tmp.write(header.length + content.length + 2);
        tmp.write(header);
        tmp.write(content.length + 1);
        tmp.write(0);
        tmp.write(str.getBytes());
        tmp.write(0);
        outputStream.write(tmp.toByteArray());
        outputStream.flush();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        mWebSocketClient.close();
    }

    WebSocketClient mWebSocketClient;
    OutputStream outputStream = null;

    private void connectWebSocket() {
        URI uri;
        try {
            uri = new URI("ws://socket_address");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                mWebSocketClient.send("Hello from android");
            }

            @Override
            public void onMessage(final String s) {
                final String message = s;
                if (outputStream == null) {
                    BluetoothAdapter blueAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (blueAdapter != null && blueAdapter.isEnabled()) {
                        Set<BluetoothDevice> bondedDevices = blueAdapter.getBondedDevices();
                        for (BluetoothDevice device : bondedDevices) {
                            device.getName();
                            if (device.getName().equals("EV3")) {
                                ParcelUuid[] uuids = device.getUuids();
                                BluetoothSocket socket = null;
                                try {
                                    socket = device.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                                    socket.connect();
                                    outputStream = socket.getOutputStream();
                                    writeMsg(outputStream, s);
                                    outputStream.flush();
                                } catch (Exception e) {
                                    try {
                                        e.printStackTrace();
                                        outputStream=null;
                                        outputStream.close();
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }else{
                    try {
                        writeMsg(outputStream, s);
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            outputStream=null;
                            outputStream.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }
}