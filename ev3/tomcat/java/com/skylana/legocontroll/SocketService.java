/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skylana.legocontroll;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Skull
 */
@ServerEndpoint("/echo")
public class SocketService {

    private static final Set<Session> SESSIONS = Collections.newSetFromMap(new ConcurrentHashMap<Session, Boolean>());

    @OnMessage
    public void echoTextMessage(Session session, String msg, boolean last) {
        SESSIONS.add(session);
        for (Session ses : SESSIONS) {
            try {
                if (ses.isOpen()) {
                    ses.getBasicRemote().sendText(msg, last);
                } else {
                    SESSIONS.remove(ses);
                }
            } catch (IOException e) {
                try {
                    session.close();
                } catch (IOException e1) {
                    // Ignore
                }
            }
        }
    }
    
    @OnClose
    public void onClose(Session session){
        SESSIONS.remove(session);
    }

}
