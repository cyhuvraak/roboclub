# Репозиторій Lviv RoboGarage #

# Робосумо #

* [Діагностика](sumo/diagnostics) 
  sumo/diagnostics Приклад тестування сенсорів та двигунів.
* [Ухилятор](sumo/yhuljator)
  sumo/yhuljator Кльова прошивка, робот буде гасати по кімнаті, повертаючи перед перешкодами.
* [Ізолєнтій-1](sumo/izolentij1)
  sumo/izolentij1 Приклад коду робота сумоїста. Прошивка переможця перших змагань
* [ESP](sumo/esp)
	* sumo/esp/esp Прошивка ESP8266 для передачі UDP мультикаст пакетів по серіалу і в зворотньому напрямі
	* sumo/esp/UDP Андроїд програма для керування сумоїстом


# [На вікі ->](https://bitbucket.org/cyhuvraak/roboclub/wiki/Home) #